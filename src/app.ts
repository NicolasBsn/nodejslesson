import express from 'express'
import metricsRouter from './routes/metrics.route'
import userRouter from './routes/users.route'


import cors = require('cors');
import path = require('path');
import logger = require('morgan');
import session = require('express-session')
import levelSession = require('level-session-store')


const LevelStore = levelSession(session)


const app = express()


// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))
app.use(session({
  secret: 'my very secret phrase',
  store: new LevelStore('./db/sessions'),
  resave: true,
  saveUninitialized: true,
}))

app.use('/metrics', metricsRouter)
app.use('/users', userRouter)


app.listen(30002, () => {
  // eslint-disable-next-line no-console
  console.log('App listening!')
})

export { app }
