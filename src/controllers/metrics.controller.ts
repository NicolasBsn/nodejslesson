import WriteStream from 'level-ws'
import { LevelDB } from '../leveldb'

export class Metric {
  public timestamp: String

  public group: String

  public value: Number

  constructor(date: String, val: Number, group: String) {
    this.timestamp = date
    this.value = val
    this.group = group
  }
}

export class MetricsHandler {
  public db: any

  constructor(dbPath: string) {
    // LevelDB.clear(dbPath)
    this.db = LevelDB.open(dbPath)
  }


  public save(key: String, group: String, metrics: Metric[],
    callback: (error: Error | null) => void) {
    const stream = WriteStream(this.db)
    stream.on('error', (err: Error) => {
      callback(err)
    })
    stream.on('close', callback)
    metrics.forEach((m: Metric) => {
      stream.write({ key: `metric:${key}:${m.timestamp}:${group}`, value: m.value })
    })
    stream.end()
  }

  public deleteOne(key: String, callback: (error: Error | null) => void) {
    this.db.createReadStream()
      .on('data', (data: any) => {
        const keyUser: String = data.key.split(':')[1]
        if (keyUser === key) {
          this.db.del(data.key).catch((err: Error) => callback(err))
        }
      })
      .on('error', (err: Error) => {
        callback(err)
      })
      .on('end', () => {
        callback(null)
      })
  }

  public deleteOneWithTimestamp(key: String, keyTimestamp: String, group: String,
    callback: (error: Error | null) => void) {
    this.db.createReadStream()
      .on('data', (data: any) => {
        const groupKey: String = data.key.split(':')[3]
        const timestamp: String = data.key.split(':')[2]
        const keyUser: String = data.key.split(':')[1]
        if (keyUser === key && keyTimestamp === timestamp && groupKey === group) {
          this.db.del(data.key).catch((err: Error) => callback(err))
        }
      })
      .on('error', (err: Error) => {
        callback(err)
      })
      .on('end', () => {
        callback(null)
      })
  }

  public getAll(callback: (error: Error | null, result: any | null) => void) {
    const metrics: Metric[] = []
    this.db.createReadStream()
      .on('data', (data: any) => {
        const timestamp: String = data.key.split(':')[2]
        const group: String = data.key.split(':')[3]
        const metric: Metric = new Metric(timestamp, data.value, group)
        metrics.push(metric)
      })
      .on('error', (err: Error) => {
        callback(err, null)
      })
      .on('end', () => {
        callback(null, metrics)
      })
  }

  public getOne(key: String, callback: (error: Error | null, result: any | null) => void) {
    const metrics: Metric[] = []
    this.db.createReadStream()
      .on('data', (data: any) => {
        const keyUser: String = data.key.split(':')[1]
        const timestamp: String = data.key.split(':')[2]
        const group: String = data.key.split(':')[3]
        if (keyUser === key) {
          const metric: Metric = new Metric(timestamp, data.value, group)
          metrics.push(metric)
        }
      })
      .on('error', (err: Error) => {
        callback(err, null)
      })
      .on('end', () => {
        callback(null, metrics)
      })
  }
}
