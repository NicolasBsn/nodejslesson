import { LevelDB } from '../leveldb'

const bcrypt = require('bcrypt')

export class User {
  public username: string

  public email: string

  public password: string = ''

  constructor(username: string, email: string, password: string) {
    this.username = username
    this.email = email
    this.password = password
  }

  static fromDb(username: string, value: any): User {
    const [password, email] = value.split(':')
    return new User(username, email, password)
  }

  public static async setPassword(toSet: String) {
    const hashedPassword = await new Promise((resolve, reject) => {
      bcrypt.hash(toSet, 5, (err: Error, hash: any) => {
        if (err) reject(err)
        resolve(hash)
      })
    })
    return hashedPassword
  }

  public getPassword(): string {
    return this.password
  }

  public validatePassword(toValidate: String) {
    return bcrypt.compareSync(toValidate, this.password)
  }
}

export class UserHandler {
  public db: any

  constructor(path: string) {
    // LevelDB.clear(path)
    this.db = LevelDB.open(path)
  }

  public get(username: string, callback: (err: Error | null, result?: User) => void) {
    this.db.get(`user:${username}`, (err: Error, data: any) => {
      if (err) callback(err)
      else if (data === undefined) callback(null, data)
      else callback(null, User.fromDb(username, data))
    })
  }

  public async save(user: any, callback: (err: Error | null) => void) {
    const pass = await User.setPassword(user.password)
    this.db.put(`user:${user.username}`, `${pass}:${user.email}`, (err: Error | null) => {
      callback(err)
    })
  }

  public async saveCredentials(user: String, req: any, callback: (err: Error | null) => void) {
    const pass = await User.setPassword(req.body.password)
    const { email } = req.body
    this.db.put(`user:${user}`, `${pass}:${email}`, (err: Error | null) => {
      callback(err)
    })
  }

  public delete(username: string, callback: (err: Error | null) => void) {
    this.db.createReadStream()
      .on('data', (data: any) => {
        const keyUser: String = data.key.split(':')[1]
        if (keyUser === username) {
          this.db.del(data.key).catch((err: Error) => callback(err))
        }
      })
      .on('error', (err: Error) => {
        callback(err)
      })
      .on('end', () => {
        callback(null)
      })
  }
}
