import * as express from 'express'
// eslint-disable-next-line no-unused-vars
import { UserHandler, User } from '../controllers/users.controller'

let db: string
if (process.env.NODE_ENV === 'test') {
  db = './db_test_users'
} else {
  db = './db/users'
}

const dbUser: UserHandler = new UserHandler(db)
const router = express.Router()

function authCheck(req: any, res: any, next: any) {
  if (req.session.loggedIn && req.session.user.username === req.body.username) {
    next()
  } else res.status(401).json({ success: false, message: 'You must be connected' })
}

router.post('/user/login', (req: any, res: any) => {
  dbUser.get(req.body.username, (err: Error | null, result?: User) => {
    if (err || result === undefined || !result.validatePassword(req.body.password)) {
      res.json({ success: false })
    } else {
      req.session.loggedIn = true
      req.session.user = result
      res.json({ success: true })
    }
  })
})

router.post('/user/signup', (req: any, res: any) => {
  dbUser.get(req.body.username, (err: Error | null, result?: User) => {
    if (!err || result !== undefined) {
      res.json({ success: 'user already exists' })
    } else {
      // eslint-disable-next-line no-shadow
      dbUser.save(req.body, (err) => {
        if (err) res.json({ success: false })

        else res.json({ success: true })
      })
    }
  })
})

router.post('/user/credentials', authCheck, (req: any, res: any) => {
  dbUser.get(req.body.username, (err: Error | null, result?: User) => {
    if (err || result === undefined) {
      res.status(404).send('user not found')
    } else {
      const { username } = result
      dbUser.delete(username, () => {
        if (err === undefined) {
          res.status(404).send('user not found')
        } else {
          dbUser.saveCredentials(username, req, () => {
            if (err) res.json({ success: false })
            else res.json({ success: true })
          })
        }
      })
    }
  })
})

router.get('/user/:username', (req: any, res: any) => {
  dbUser.get(req.params.username, (err: Error | null, result?: User) => {
    if (err || result === undefined) {
      res.status(404).send('user not found')
    } else res.status(200).json(result)
  })
})

router.delete('/user/:username', (req: any, res: any) => {
  dbUser.delete(req.params.username, (err: Error | null) => {
    if (err === undefined) {
      res.status(404).send('user not found')
    } else res.status(200).json({ success: true })
  })
})

router.get('/logout', (req: any, res: any) => {
  delete req.session.loggedIn
  delete req.session.user
  res.json({ success: true })
})


export default router
