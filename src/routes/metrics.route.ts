import * as express from 'express'
import { MetricsHandler } from '../controllers/metrics.controller'

let db: string
if (process.env.NODE_ENV === 'test') {
  db = './db_test_metrics'
} else {
  db = './db/metrics'
}

const dbMet: MetricsHandler = new MetricsHandler(db)

const router = express.Router()

function authCheck(req: any, res: any, next: any) {
  if (req.session.loggedIn && req.session.user.username === req.params.id) {
    next()
  } else res.status(401).json({ success: false, message: 'You must be connected' })
}

router.post('/:id', authCheck, (req: any, res: any) => {
  dbMet.save(req.params.id, req.query.group, req.body, (err: Error | null) => {
    if (err) {
      res.status(400).send({ error: true })
    } else {
      res.status(200).json({ success: true })
    }
  })
})

router.delete('/:id', authCheck, (req: any, res: any) => {
  dbMet.deleteOne(req.params.id, (err: Error | null) => {
    if (err) throw err
    res.status(200).json({ success: true })
  })
})

router.delete('/:id/:timestamp', authCheck, (req: any, res: any) => {
  dbMet.deleteOneWithTimestamp(req.params.id, req.params.timestamp,
    req.query.group, (err: Error | null) => {
      if (err) throw err
      res.status(200).json({ success: true })
    })
})

// For dev purpose only
/*router.get('/', authCheck, (req: any, res: any) => {
  dbMet.getAll((err: Error | null, result: any) => {
    if (err) throw err
    res.status(200).send(result)
  })
})*/

router.get('/:id', authCheck, (req: any, res: any) => {
  dbMet.getOne(req.params.id, (err: Error | null, result: any) => {
    if (err) throw err
    else {
      res.status(200).send(result)
    }
  })
})


router.get('*', (req: any, res: any) => {
  res.send('Unkown page', 404)
})

export default router
