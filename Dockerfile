FROM node:latest as test
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN npm run pre-install
# There is an issue with the module bcrypt that needs to be rebuilt and update with its binary file
RUN npm rebuild bcrypt --update-binary
RUN npm run populate
EXPOSE 30002 30003
CMD npm test

FROM test as prod
CMD npm start
