#!/usr/bin/env ts-node

import { Metric, MetricsHandler } from '../src/controllers/metrics.controller'
import { User, UserHandler } from '../src/controllers/users.controller'


const users = [
  new User('Nicolas', 'nicolas@gmail.com', '123'),
  new User('Alek6', 'alexisse@gmail.com', '123'),
]

const metNicolasA = [
  new Metric('2016-12-22-14-50-30', 12, 'A'),
  new Metric('2018-09-22-13-55-20', 10, 'A'),
  new Metric('2019-12-22-14-38-29', 89, 'A'),
]
const metNicolasB = [
  new Metric('2016-12-22-14-50-30', 80, 'B'),
  new Metric('2018-09-22-13-55-20', 120, 'B'),
  new Metric('2019-12-22-14-38-29', 200, 'B'),
]
const metNicolasC = [
  new Metric('2016-12-22-14-50-30', 9, 'C'),
  new Metric('2018-09-22-13-55-20', 3, 'C'),
  new Metric('2019-12-22-14-38-29', 20, 'C'),
]

const metAlekssisseA = [
  new Metric('2016-12-22-14-50-30', 12, 'A'),
  new Metric('2018-09-22-13-55-20', 10, 'A'),
  new Metric('2019-12-22-14-38-29', 89, 'A'),
]
const metAlekssisseB = [
  new Metric('2016-12-22-14-50-30', 80, 'B'),
  new Metric('2018-09-22-13-55-20', 120, 'B'),
  new Metric('2019-12-22-14-38-29', 200, 'B'),
]
const metAlekssisseC = [
  new Metric('2016-12-22-14-50-30', 9, 'C'),
  new Metric('2018-09-22-13-55-20', 3, 'C'),
  new Metric('2019-12-22-14-38-29', 20, 'C'),
]


const dbMet = new MetricsHandler('./db/metrics')

const dbUser = new UserHandler('./db/users')


users.forEach((user: User) => {
  dbUser.save(user, (err: Error | null) => {
    if (err) throw err
  })
})

dbMet.save('Nicolas', 'A', metNicolasA, (err: Error | null) => {
  if (err) throw err
})
dbMet.save('Nicolas', 'B', metNicolasB, (err: Error | null) => {
  if (err) throw err
})
dbMet.save('Nicolas', 'C', metNicolasC, (err: Error | null) => {
  if (err) throw err
})
dbMet.save('Alek6', 'A', metAlekssisseA, (err: Error | null) => {
  if (err) throw err
})
dbMet.save('Alek6', 'B', metAlekssisseB, (err: Error | null) => {
  if (err) throw err
})
dbMet.save('Alek6', 'C', metAlekssisseC, (err: Error | null) => {
  if (err) throw err
})
