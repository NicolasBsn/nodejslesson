import chai, { expect } from 'chai'

import chaiHttp from 'chai-http'
import { app } from '../src/app'


import { Metric, MetricsHandler } from '../src/controllers/metrics.controller'
import { User, UserHandler } from '../src/controllers/users.controller'
import { LevelDB } from '../src/leveldb'

const should = chai.should()

chai.use(chaiHttp)
const dbPath: string = 'db_test_metrics'
const dbPathUser: string = 'db_test_users'
let dbMet: MetricsHandler
let dbUser: UserHandler
const agent = chai.request.agent(app)

describe('Users', () => {
  before(() => {
    LevelDB.clear(dbPathUser)
    dbUser = new UserHandler(dbPathUser)
  })

  after(() => {
    dbUser.db.close()
  })

  describe('#save with DB', () => {
    it('should save user', (done) => {
      const user = new User('nico', '20/20AtWebTech@gmail', 'mypass')
      dbUser.save(user, (err: Error | null) => {
        expect(err).to.be.undefined
        done()
      })
    })
  })

  describe('#save with api', () => {
    it('should save user', (done) => {
      chai.request(app)
        .post('/users/user/signup')
        .send({ username: 'nicolas1', email: '21/20AtWebTech@gmail', password: '123' })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('success').eql(true)
          done()
        })
    })
  })

  describe('#login with api', () => {
    it('should login', (done) => {
      chai.request(app)
        .post('/users/user/login')
        .send({ username: 'nicolas1', password: '123' })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('success').eql(true)
          done()
        })
    })
  })

  describe('#Change credentials with api', () => {  
    beforeEach((done) => {
      agent
        .post('/users/user/login')
        .send({ username: 'nicolas1', password: '123' })
        .then(() => {
          done()
        })
    })
    it('should change email and password', (done) => {
      agent
        .post('/users/user/credentials')
        .send({ username: 'nicolas1', password: '1234' })
        .then((res) => {
          res.should.have.status(200)
          res.body.should.have.property('success').eql(true)
          done()
        })
    })
  })

  describe('#logout with api', () => {
    it('should logout', (done) => {
      chai.request(app)
        .get('/users/logout')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('success').eql(true)
          done()
        })
    })
  })

  describe('#delete with api', () => {
    it('should delete the user', (done) => {
      chai.request(app)
        .delete('/users/user/nicolas1')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('success').eql(true)
          done()
        })
    })
  })

  describe('#get with db', () => {
    it('should get the User', (done) => {
      dbUser.get('nico', (err: Error | null, result?: User) => {
        expect(err).to.be.null
        expect(result).to.not.be.undefined
        done()
      })
    })
  })

  describe('#delete with db', () => {
    it('should delete the existing user', (done) => {
      dbUser.delete('nico', (err: Error | null, result?: any) => {
        expect(err).to.be.null
        done()
      })
    })
  })
})


describe('Metrics', () => {
  before((done) => {
    LevelDB.clear(dbPath)
    dbMet = new MetricsHandler(dbPath)
    chai.request(app)
      .post('/users/user/signup')
      .send({ username: 'nicolas1', email: '21/20AtWebTech@gmail', password: '123' })
      .end(() => {
        done()
      })
  })

  beforeEach((done) => {
    agent
      .post('/users/user/login')
      .send({ username: 'nicolas1', password: '123' })
      .then(() => {
        done()
      })
  })

  after(() => {
    dbMet.db.close()
  })

  describe('#get with api', () => {
    it('should post array of data', (done) => {
      agent
        .post('/metrics/nicolas1?group=a')
        .send([{ timestamp: '2001-01-10-23-34', value: 10 }])
        .then((res) => {
          expect(res).to.have.status(200)
          res.body.should.have.property('success').eql(true)
          done()
        })
    })

    it('should return an array', (done) => {
      agent
        .get('/metrics/nicolas1')
        .then((res) => {
          expect(res).to.have.status(200)
          expect(res.body[0].value).to.eql(10)
          done()
        })
    })

    it('should return error as this user is not logged in', (done) => {
      agent
        .get('/metrics/nicolas2')
        .then((res) => {
          expect(res).to.have.status(401)
          res.body.should.have.property('message').eql('You must be connected')
          done()
        })
    })
  })

  describe('#get with db', () => {
    it('should get empty array on non existing group', (done) => {
      dbMet.getOne('0', (err: Error | null, result?: Metric[]) => {
        expect(err).to.be.null
        expect(result).to.not.be.undefined
        expect(result).to.be.empty
        done()
      })
    })

    it('should get array of data', (done) => {
      const met: Metric[] = []
      met.push(new Metric('111110998', 10, 'a'))
      dbMet.save('1', 'a', met, (err: Error | null) => {
        dbMet.getOne('1', (err: Error | null, result?: Metric[]) => {
          expect(err).to.be.null
          expect(result).to.not.be.empty
          if (result) expect(result[0].value).to.have.equal(10)
          done()
        })
      })
    })
  })

  describe('#save with DB', () => {
    it('should save data', (done) => {
      const met = [
        new Metric(`${new Date('2013-11-04 14:00 UTC').getTime()}`, 12, 'a'),
        new Metric(`${new Date('2013-11-04 14:15 UTC').getTime()}`, 10, 'a'),
        new Metric(`${new Date('2013-11-04 14:30 UTC').getTime()}`, 8, 'a'),
      ]
      dbMet.save('0', 'a', met, (err: Error | null) => {
        expect(err).to.be.undefined
        done()
      })
    })
  })

  describe('#delete with API', () => {
    it('should not fail as data do not exist', (done) => {
      agent
        .delete('/metrics/nicolas1/2019-09-10-10-55-55?group=a')
        .then((res) => {
          expect(res).to.have.status(200)
          res.body.should.have.property('success').eql(true)
          done()
        })
    })
  })

  describe('#delete with db', () => {
    it('should delete existing data', (done) => {
      dbMet.deleteOne('1', (err: Error | null, result?: any) => {
        expect(err).to.be.null
        done()
      })
    })
  })
})

