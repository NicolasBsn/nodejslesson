import React from 'react';
import ReactDOM from 'react-dom';
import LogIn from './LogIn';
import SignUp from './SignUp';
import Homepage from './Homepage';
import ChangeCredentials from './ChangeCredentials'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

const routing = (
  <Router>
    <Switch>
      <Route exact path="/" component={LogIn} />
      <Route exact path="/signup" component={SignUp} />
      <Route exact path="/homepage" component={Homepage} />
      <Route exact path="/modify-account" component={ChangeCredentials} />
    </Switch>
  </Router>
);
// eslint-disable-next-line no-undef
ReactDOM.render(routing, document.getElementById('root'));
