import React from 'react';
import axios from 'axios';
import './LogIn.css';
import {
  Input,
  Container,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Alert,
  Row,
  Col,
  Form,
  FormGroup,
} from 'reactstrap';

export default class LogIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      username: '',
      alert: '',
      usersList: JSON.parse(localStorage.getItem('users')),
    };
  }

  // eslint-disable-next-line react/sort-comp
  handleOnChangeEmail(event) {
    this.setState({
      email: event.target.value,
    });
  }

  handleOnChangePassword(event) {
    this.setState({
      password: event.target.value,
    });
  }

  handleOnChangeUsername(event) {
    this.setState({
      username: event.target.value,
    });
  }

  signIn() {
    axios.post('/users/user/login', {'username': this.state.username, 'password': this.state.password,'email': this.state.email})
    .then(res => {
      if(res.data.success === true){
        localStorage.setItem('username', this.state.username);
        this.props.history.push('/homepage');
      }
      else{
        this.setState({
          alert: <Alert color="danger">Password or username must be wrong</Alert>,
        });
      }
    })
  }

  setRedirect() {
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/signup');
  }

  render() {
    return (
      <Container>
        <Card className="card-layout">
          <CardBody>
            <CardTitle className="card-title">
              <div className="title">
              <span className="water">My</span>
                <span className="melon">Metrics</span>
              </div>
            </CardTitle>
            <CardSubtitle className="card-subtitle">Identification</CardSubtitle>
            <Form>
              <FormGroup>
                <Input
                  placeholder="Username"
                  onChange={e => this.handleOnChangeUsername(e)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  type="password"
                  name="password"
                  id="examplePassword"
                  placeholder="Password"
                  onChange={e => this.handleOnChangePassword(e)}
                />
              </FormGroup>
            </Form>
            <Row>
              <Col>
                <Button id="button" color="primary" onClick={() => this.signIn()}>
                  Sign in
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button id="button" color="light" onClick={() => this.setRedirect()}>
                  New user
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>{this.state.alert}</Col>
            </Row>
          </CardBody>
        </Card>
      </Container>
    );
  }
}
