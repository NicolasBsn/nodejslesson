import React, { Component } from 'react';
import {
  Container,
  Card,
  CardBody,
  CardTitle,
  Button,
  Row,
  Col,
  Input,
  CardText,
} from 'reactstrap';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import axios from 'axios';
import './Homepage.css';

class App extends Component {

  constructor(props){
    super(props);
    this.state={ metricsA: [], metricsB: [], metricsC: [], value:''};
    this.displayListMetrics = this.displayListMetrics.bind(this);
  }

  componentWillMount() {
    this.renderMyData();
  }

  async renderMyData(){
    const username = localStorage.getItem('username');
    const res = await axios.get(`/metrics/${username}`);
    this.setState({
      metricsA: res.data.filter(metric => metric.group === 'A'),
      metricsB: res.data.filter(metric => metric.group === 'B'),
      metricsC: res.data.filter(metric => metric.group === 'C')
    });
  }

  updateMetric(metric){
    if(this.state.value <= 300 && this.state.value !== ''){
      const username = localStorage.getItem('username');
      axios.post(`/metrics/${username}?group=${metric.group}`,[{'timestamp': metric.timestamp, 'value': this.state.value}])
      .then(res => {
        this.renderMyData();
      })
    }
    else{
      alert("The value must be a number lower or equal than 300");
    }
  }

  postMetrics(group){
    if(this.state.value <= 300 && this.state.value !== ''){
      const username = localStorage.getItem('username');
      var now = new Date(); 
      var year = now.getFullYear();
      var month = now.getMonth()+1; 
      var day = now.getDate();
      var hour = now.getHours();
      var minute = now.getMinutes();
      var second = now.getSeconds(); 
      if(month.toString().length === 1) {
        month = '0'+month;
      }
      if(day.toString().length === 1) {
        day = '0'+day;
      }   
      if(hour.toString().length === 1) {
        hour = '0'+hour;
      }
      if(minute.toString().length === 1) {
        minute = '0'+minute;
      }
      if(second.toString().length === 1) {
        second = '0'+second;
      } 
      var dateTime = year+'-'+month+'-'+day+'-'+hour+'-'+minute+'-'+second;  
      axios.post(`/metrics/${username}?group=${group}`,[{'timestamp': dateTime, 'value': this.state.value}])
      .then(res => {
        this.renderMyData();
      })
    }
    else{
      alert("The value must be lower or equal than 300");
    }
  }
  
  deleteMetric = metric => {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    const username = localStorage.getItem('username');
    axios.delete(`/metrics/${username}/${metric.timestamp}?group=${metric.group}`,headers)
    .then(res => {
      this.renderMyData();
    })
  }

  async deleteUser(){
    const username = localStorage.getItem('username');
    await axios.delete(`/users/user/${username}`);
    localStorage.removeItem('username');
    this.props.history.push('/');
  }

  async logOut(){
    localStorage.removeItem('username');
    await axios.get(`/users/logout`);
    this.props.history.push('/');
  }

  changeCredential(){
    this.props.history.push('/modify-account');
  }


  handleOnChangeValue(event) {
    this.setState({
      value: event.target.value,
    });
  }

  displayListMetrics(metric) {
    return (
      <Card key={metric.timestamp} className="card-layout">
        <CardText>
          <Row>
            <Col>
              <span className="water">Timestamp (aaaa-mm-dd-hh-mm-ss):</span> <span className="melon">{metric.timestamp}</span>
            </Col>
          </Row>
          <Row>
            <Col>
              <span className="water">Value :</span> <span className="melon">{metric.value}</span>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button id="button" onClick={() => this.deleteMetric(metric)} color="danger" size="sm">
                Delete
              </Button>
            </Col>
            <Col>
              <Button id="button" color="info" size="sm" onClick={() => this.updateMetric(metric)}>Update</Button>
            </Col>
          </Row>
        </CardText>
      </Card>
    );
  }


  render() {
    if(localStorage.getItem('username') === null){
      this.props.history.push('/');
    }
    return (
    <Container>
        <Row>
          <Col>
            <Button id="button" color="info" size="sm" onClick={() => this.changeCredential()}>
              Modify account
            </Button>
          </Col>
          <Col>
            <Button id="button" color="danger" size="sm" onClick={() => this.deleteUser()}>
              Delete my Account
            </Button>
          </Col>
          <Col>
            <Button id="button" color="secondary" size="sm" onClick={() => this.logOut()}>
              Log Out
            </Button>
          </Col>
          <Col></Col>
          <Col></Col>
          <Col></Col>
        </Row>
        <Card className="card-layout">
          <div className="title">
              <span className="water">My</span>
              <span className="melon">Dashboard</span>
          </div>
          <CardBody>
            <Row>
            <CardTitle className="card-subtitle">
                <div className="title">
                <span className="water">My</span>
                <span className="melon">Height</span>
                </div>
              </CardTitle>
              <CardTitle className="card-subtitle">
                <div className="title">
                <span className="water">My</span>
                <span className="melon">Weight</span>
                </div>
              </CardTitle>
              <CardTitle className="card-subtitle">
                <div className="title">
                <span className="water">My</span>
                <span className="melon">Sleep</span>
                </div>
              </CardTitle>
            </Row>
            <Row>
              <LineChart
                width={325}
                height={400}
                data={this.state.metricsA}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="timestamp"/>
                  <YAxis type="number" domain={[0, 300]} />
                  <Tooltip />
                  <Legend />
                  <Line name="Height in Cm" type="monotone" dataKey="value" stroke="#8884d8"/>
              </LineChart>
              <LineChart
                width={325}
                height={400}
                data={this.state.metricsB}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="timestamp"/>
                  <YAxis type="number" domain={[0, 300]} />
                  <Tooltip />
                  <Legend />
                  <Line name="Weight in Kg" type="monotone" dataKey="value" stroke="#82ca9d"/>
              </LineChart>
              <LineChart
                width={325}
                height={400}
                data={this.state.metricsC}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="timestamp"/>
                  <YAxis type="number" domain={[0, 24]} />
                  <Tooltip />
                  <Legend />
                  <Line name="Hours of Sleep" type="monotone" dataKey="value" stroke="#F8340A"/>
              </LineChart>
            </Row>
            <Row>
            <Col sm={{ size: 6, offset: 3 }}>
                Enter a value and either <span className="water">post</span> or <span className="melon">update</span> your metrics : <Input placeholder="0 < Value < 300" type="number" onChange={e => this.handleOnChangeValue(e)}/>
            </Col>
            </Row>
            <Row>
              <Col>
                <Button id="button" color="primary" onClick={() => this.postMetrics('A')}>
                  Post your Height
                </Button>
              </Col>
              <Col>
                <Button id="button" color="primary" onClick={() => this.postMetrics('B')}>
                  Post your Weight
                </Button>
              </Col>
              <Col>
                <Button id="button" color="primary" onClick={() => this.postMetrics('C')}>
                  Post your hours of Sleep
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>
              {this.state.metricsA.map(this.displayListMetrics)}
              </Col>
              <Col>
              {this.state.metricsB.map(this.displayListMetrics)}
              </Col>
              <Col>
              {this.state.metricsC.map(this.displayListMetrics)}
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Container>
    );
  }
}

export default App;
