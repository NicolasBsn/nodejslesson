import React from 'react';
import axios from 'axios';
import './LogIn.css';
import {
  Input,
  Container,
  Card,
  CardBody,
  CardTitle,
  Button,
  Row,
  Alert,
  Col,
  Form,
  FormGroup,
  CardSubtitle,
} from 'reactstrap';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      username: '',
      alert: '',
    };
  }

  createUser = () => {
    if (
      this.state.email === '' ||
      this.state.password === '' ||
      this.state.username === ''
    ) {
      this.setState({
        alert: <Alert color="danger">Please, fill all the fields</Alert>,
      });
    } 
    else {
      const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      axios.post('/users/user/signup',{'username': this.state.username, 'password': this.state.password,'email': this.state.email}, headers)
      .then(res => {
        if(res.data.success === true){
          localStorage.setItem('username', this.state.username);
          this.props.history.push('/');
        }
        else{
          this.setState({
            alert: <Alert color="danger">An error occured</Alert>,
          });
        }
      })
    }
  };

  handleOnChangeEmail(event) {
    this.setState({
      email: event.target.value,
    });
  }

  handleOnChangePassword(event) {
    this.setState({
      password: event.target.value,
    });
  }

  handleOnChangeUsername(event) {
    this.setState({
      username: event.target.value,
    });
  }
  
  render() {
    return (
      <Container>
        <Card className="card-layout">
          <CardBody>
            <CardTitle className="card-title">
              <div className="title">
                <span className="water">My</span>
                <span className="melon">Metrics</span>
              </div>
            </CardTitle>
            <CardSubtitle className="card-subtitle">Registration</CardSubtitle>
            <Form>
            <FormGroup>
                <Input
                  placeholder="Username"
                  onChange={e => this.handleOnChangeUsername(e)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  type="email"
                  name="email"
                  placeholder="Email"
                  onChange={e => this.handleOnChangeEmail(e)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  type="password"
                  name="password"
                  id="examplePassword"
                  placeholder="Password"
                  onChange={e => this.handleOnChangePassword(e)}
                />
              </FormGroup>
            </Form>
            <Row>
              <Col>
                <Button id="button" color="primary" onClick={this.createUser}>
                  Create Account
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>{this.state.alert}</Col>
            </Row>
          </CardBody>
        </Card>
      </Container>
    );
  }
}

export default SignUp;
