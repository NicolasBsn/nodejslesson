# NodeJSLesson : My Metrics :chart_with_upwards_trend:

This is an app to post and retrieve your metrics. The purpose of its creation was for the Web Development and the DevOps courses of ING4 at ECE Paris.

## How to use

### With Node

Just ```npm run pre-install``` (:warning: :warning: NOT ```npm install``` that will only install the ```node_modules``` of the express-server without getting the ones required for the front end), and ```npm start``` to start the server and the front.


### With Docker

#### If you clone the repository

You can run the application with docker by simply using ```npm run start-docker-prod```.
YOu can also run the tests inside a docker container by doing ```npm run start-docker-test``` and do a ```sudo docker logs [CONTAINER_ID]```.

#### If you pull the docker image from the public GitLab registry
You can also run the application directly by pulling the image from my registry with ```sudo docker pull registry.gitlab.com/nicolasbsn/nodejslesson```.
And run it with ```sudo docker run -p 30002:30002 -p 30003:30003 -d registry.gitlab.com/nicolasbsn/nodejslesson```.


Front-side is in React.

Server-side is in Express.

## API REST

This API is a REST-API that is using JSON as outputs and inputs.

## API Routes

- ```post``` **/users/user/signup** allows you to create a new user with an ```username```, an ```email``` and a ```password```
- ```post``` **/users/user/login** allows you to login with your credentials given an ```username``` and a ```password```
- ```post``` **/users/user/credentials** allows you to change your credentials given an ```username``` a ```password``` and an ```email```
- ```delete``` **/users/user/:username** allows you to delete an anccount given an ```username```
- ```get``` **/users/user/logout** allow you to log out.
- ```get``` **/metrics/:id** will give you metrics within a JSON, given a particular ```id```
- ***Commented Route*** ```get``` **/metrics** will give you all the metrics within a JSON
- ```post``` **/metrics/:id?group=A** will post you metrics with a JSON as a body, given a particular ```id``` and ```group``` (here the group is ```'A'```). You need to give a field ```timestamp```  and ```value```.
- an Unkown route will tell you that the route doesn't exist.

***Note*** : The route ```get``` **/metrics** is only there for dev purposes, do not use it ! 

## Front-side Routes

- **/** will redirect you to the index where you can log-in
- **/signup** will redirect you to the signup page
- **/homepage** will redirect to your dashboard where you can retrieve your metrics, add new ones, log-out and delete your account

You can perfom all the actions listed above. You can retrieve your metrics, displayed in 3 Graphs : MySleep (your hours of sleep), MyHeight, MyWeight. Below each graph, you can add new metrics, delete or update previous ones.


## Example

- ```get``` **/metrics/nicolas** would trigger ```[{"timestamp":1383573600000,"value":12, "group":"A"}]```
- ```get``` **/metrics would trigger** ```[{"timestamp":1383573600000,"value":12, "group":"A"},{"timestamp":1383575400000,"value":15, "group":"B"}]```

## CI/CD

The project is tested with mocha and chai. When code is committed and pushed, thanks to Gitlab CI, the code is tested on shared runners, inside a docker and then the application is packaged to be pushed into the gitlab container registry.

## Test coverage

All the most important features are tested ! User CRUD and Metrics CRUD.

## Future improvements

We could think about hosting the Database instead of having it in the project folder. We could also deploy the application in the Cloud, thanks to Kubernetes that is using containers.

## Versions

You can check the evolution of the features by visiting the different versions of the project.

## Documention

The project is using :

- ExpressJs
- Some useful middlewares (morgan, body-parser etc..)
- Mocha / Chai
- TypeScript
- ReactJs
- Reactstrap
- Axios
- Recharts

## Contributor(s)

Nicolas Boussenina ECE Paris

